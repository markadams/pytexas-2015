# Slides from Building Python Web Apps with Docker

This repository contains my slides from my PyTexas 2015 talk on Python and Docker.

## Abstract
If you haven't heard of Docker yet, its a great tool that allows you wrap up your app and everything it needs to run: code, runtime, and even system libraries and guarantee that it will always run the same, regardless of the environment (local machine, server, or even the cloud). Whether you're deploying a web app, performing data analysis, or creating local environments for your dev team or CI builds, Docker can help.

I'll give an introduction to Docker, an overview of some of the current tools in the Docker ecosystem (Docker Machine and Docker Compose) and demonstrate how to create, build, and deploy Python applications using Docker.

This talk is targeted towards web developers, data scientists, or really anyone who develops using Python that would like to learn more about Docker and how it can help their projects.

## Demo Application
The source for the demo application is located at:

* [https://bitbucket.org/markadams/pytexas-2015-demo](https://bitbucket.org/markadams/pytexas-2015-demo)
